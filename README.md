# LSTM Predictive Network

An LSTM network for predicting a sequence of satellite images for changes
ground deformation. This is the program used for my masters project.

## To Run

Follow the link to google colab to run the program:

https://colab.research.google.com/drive/1Wus0LG34cWH_c8aCEenJQ1S86VT2eYag?usp=sharing
